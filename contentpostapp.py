from webapp import WebApp
import argparse


PORT = 1234

PAGE = """
<!DOCTYPE html>
<html lang="en">
    <body>
    <form action="" method="post">
        <label for="content">Update content:</label>
        <input type="text" id="content" name="content"><br><br>
        <input type="submit" value="Post">
    </form>
    {content}
  </body>
</html>
"""

PAGE_NOT_FOUND = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Resource not found: {resource}.</h1>
    <form action="" method="post">
        <label for="content">Update content:</label>
        <input type="text" id="content" name="content"><br><br>
        <input type="submit" value="Post">
    </form>
  </body>
</html>
"""


def parse_args():
    parser = argparse.ArgumentParser(description="Simple HTTP Server")
    parser.add_argument('-p', '--port', type=int, default=PORT,
                        help="TCP port for the server")
    arg = parser.parse_args()
    return arg


class ContentPostApp(WebApp):
    def __init__(self, hostname, port):
        self.contents = {}
        super().__init__(hostname, port)

    def parse(self, request):
        content = request.split('\r\n\r\n')[1]
        parsed_request = request.split()[1]
        parsed_method = request.split()[0]
        return parsed_request, parsed_method, content

    def process(self, parsed_request, parsed_method, content):
        if parsed_method == 'GET':
            if parsed_request in self.contents:
                html = PAGE.format(content=self.contents[parsed_request])
                print("Process: Returning 200 OK")
                return "200 OK", html
            else:
                return "404 Not Found", PAGE_NOT_FOUND.format(resource=parsed_request)
        elif parsed_method == 'PUT':
            self.contents[parsed_request] = content
            html = PAGE.format(content=self.contents[parsed_request])
            return "200 OK", html
        elif parsed_method == 'POST':
            if '+' in content:
                content = ' '.join(str(content.split('=')[1]).split('+'))
                self.contents[parsed_request] = content
            else:
                content = (str(content.split('=')[1]))
                self.contents[parsed_request] = content
            html = PAGE.format(content=self.contents[parsed_request])
            return "200 OK", html


if __name__ == "__main__":
    args = parse_args()
    app = ContentPostApp('localhost', args.port)